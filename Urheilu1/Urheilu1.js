// HENKILO-luokalla on...
class Henkilo {
  constructor(etunimet, sukunimi, kutsumanimi, syntymavuosi) {
    this.etunimet = etunimet;
    this.sukunimi = sukunimi;
    this.kutsumanimi = kutsumanimi;
    this.syntymavuosi = syntymavuosi;
  }

  get getEtunimet() {
    return this.etunimet;
  }

  get getSukunimi() {
    return this.sukunimi;
  }

  get getKutsumanimi() {
    return this.kutsumanimi;
  }

  get getSyntymavuosi() {
    return this.syntymavuosi;
  }

  set setEtunimet(etunimet) {
    this.etunimet = etunimet;
  }

  set setSukunimi(sukunimi) {
    this.sukunimi = sukunimi;
  }

  set setKutsumanimi(kutsumanimi) {
    this.kutsumanimi = kutsumanimi;
  }

  set setSyntymavuosi(syntymavuosi) {
    this.syntymavuosi = syntymavuosi;
  }
}

// URHEILIJA-luokka perii luokan Henkilo
class Urheilija extends Henkilo {
  constructor(
    etunimet,
    sukunimi,
    kutsumanimi,
    syntymavuosi,
    kuvalinkki,
    omapaino,
    laji,
    saavutukset
  ) {
    super(etunimet, sukunimi, kutsumanimi, syntymavuosi);
    this.kuvalinkki = kuvalinkki;
    this.omapaino = omapaino;
    this.laji = laji;
    this.saavutukset = saavutukset;
  }

  get getKuvalinkki() {
    return this.kuvalinkki;
  }

  get getOmapaino() {
    return this.omapaino;
  }

  get getLaji() {
    return this.laji;
  }

  get getSaavutukset() {
    return this.saavutukset;
  }

  set setKuvalinkki(osoite) {
    this.kuvalinkki = osoite;
  }

  set setOmapaino(paino) {
    this.omapaino = paino;
  }

  set setLaji(laji) {
    this.laji = laji;
  }

  set setSaavutukset(saavutukset) {
    this.saavutukset = saavutukset;
  }
}

const h1 = new Henkilo("Antti Martti", "Meikalainen", "Antti", 1999);
const u1 = new Urheilija(
  "Hannu-Pekka Matias",
  "Kuusinen",
  "Hannu-Pekka",
  1989,
  "www.hpkaukonen.fi",
  72,
  "pikaluistelu",
  "SM-pronssia 1998"
);

const u2 = new Urheilija(
  "Maria Annika Laura Serafiina Elina",
  "Korhonen",
  "Maria",
  1995,
  "www.mariakorhonen.fi",
  68,
  "taitoluistelu",
  "MM-hopeaa 2032"
);

console.log(h1.getEtunimet);
console.log(u2.getKuvalinkki);

h1.setKutsumanimi = "Martti";
u1.setSukunimi = "Kaukonen";
u1.setLaji = "jääkiekko";
u2.setOmapaino = 65;

console.log(h1);
console.log(u1);
console.log(u2);
